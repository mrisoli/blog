+++
date = "2016-11-06T19:11:05-02:00"
title = "Introduction to Content Marketing for Developers"
draft = false

+++
---

After working for about 18 months in a content marketing startup as a Rails Developer I got to know more than just the usual programming tasks, as the company evangelizes content marketing and supports continuous learning, I got to learn a little bit more about content and inbound marketing, and it surprised me how much it was in widespread use amongst many developers and developer-focused companies.

To some extent this seems obvious, developers are used to Googling constantly for just about anything, in the early days all you had was a language specification, now the solutions to your problems are usually a StackOverflow answer away.

## What is content Marketing?

Content Marketing is the production and curation of content to attract and generate customers, it has existed for a long time, for instance the Michelin Guide is a great example, it was created in 1900 to boost the demand for cars and tires, in the internet it manifests itself mostly through blogs, if you’ve read any company blog, you’ve been funneled through content marketing.

There are many types of content used for marketing, not just blog posts, but also e-books, videos, or infographics, in the developer world content you can also list StackOverflow questions or GitHub repos, as well as many different screencasts.

## The Developer Content Marketing World

There are two main types of developer content creator, engineering-focused companies or actual developers, each with very distinct goals.

- Companies may create developer-focused content either because they offer developer tools(notable examples are Auth0, Digital Ocean or Codeship), or if they are attempting to recruit developers by showcasing their engineering culture(such as AirBnB or Pinterest), a notable third type are platform companies who use content to tutor and evangelize its tools, including Facebook and Google.

- Individual developers should write to showcase knowledge and skill, either to improve their resumé and display work to companies or find freelance work, or to generate and maintain influence on developer communities and sell developer learning material(such as Eric Elliot, Uncle Bob), In this day and age where a GitHub profile is more valuable than a resumé contributing to open source software is a great way to market yourself.

---

## Characteristics of Developer Content

Diving through some highly notorious developer blogs I noticed a few differences from content marketing from other industries:

- Images play a much more limited role, developers do not like stock photos

- In a world where the average webpage is the size of the original Doom install making it a faster, more content focused website is key, you do not need the latest JS framework just to show a blog post, look how fast this loads.

After going through a lot of different content of all kinds, I scribbled down a list of the types of content that may appeal to developers:

- Tutorials and walkthroughs. This generates a lot of traffic specially for newer languages and frameworks(think Elixir/Phoenix or something like Meteor) where there isn’t a lot of content available, since most getting started content is very shallow, you can also create content integrating different technology, such as putting one of the many JavaScript front-end frameworks available with a back-end framework.

- Novelty tutorials, where you can make simple examples of new features of upcoming or just released versions of languages or frameworks, such as ES2015.

- Benchmarks and comparisons. Developers are always interested in what is better or faster, for this its important to provide either a fair measurement of different languages/frameworks performance or side by side code that reads better and is easier to write.

- Rants, developers love to rant, and this gets a lot of traffic, the words JavaScript Fatigue get thrown around a lot these days.

This is just a list of writable content for blogging, the most valuable type of content tailored for developers is of course code, at any point if you can provide a Github repo or some code samples this makes your content much more attractive. For tutorials a Github repo with well separated commits for each step are great for developers to learn along.

## Where to publish your content

Many developers are wary of setups, they are prone to errors and may take too much time and effort, this is why create-react-app and similar generators are growing in popularity, and also why a developer might prefer a solution that works out of the box.

This is why Medium is so popular with many developers, it requires zero-config, comments and analytics setup, no need for account creation or anything, many companies have even started using Medium Publications as their company blog, I cross-posted this to Medium [here](https://medium.com/@celorisoli/introduction-to-content-marketing-for-developers-71202a91551f#.5p7andd7l).

However if blogging is a core part of your business or activity it makes sense to go with your own, hosted CMS, in this space Wordpress remains king, it includes a large developer community supporting it, which includes a large selection of different plugins and themes to get you started.

Another great option is to use your own, static website generator, which will render your content much faster, thus making it better for SEO, in order to produce this article I checked around a few, initially I tried Jekyll and eventually I found Hugo which had great documentation and very easy setup, with a few lines of code and the help of Gitlab CI I was able to put a static blog hosted on Gitlab pages quickly, you can check the repo for this blog [here](https://gitlab.com/mrisoli/blog).

---

## Promoting your content

Among social networks it seems clear the developers tend to use Twitter the most, while promoting on Facebook and Twitter altogether won’t demand that much effort, Facebook may have a bigger audience, but many developers will flock to Twitter to engage and share content. Many professionals will often also promote(and post) content on LinkedIn, but in my experience developers usually despise LinkedIn due to its widespread usage of dark patterns.

There are also a lot of focused communities for all kinds of developers, for instance StackOverflow is a great way to engage with developers seeking for help and questions might often popup an opportunity to share you content in answers. You can also find a lot of developers in communities such as Hacker News, some subreddits and even language specific forums like Echo JS, hell, there are even open source tooling to create your own community like Telescope or Discourse.

It is important to maintain engagement with your audience as developers are keen to provide feedback and help you improve your content and your code, this is the backbone of the open source community and how many open source projects are thriving, many will highlight the efforts of vue.js creator, as of today there are over 3100 closed issues on the vue.js Github repo, his readiness to respond to his community along with great documentation is what made Vue on par with Angular and React in the JS ui libraries highly competitive space.

## Measuring your content’s performance

As of today Google Analytics is the undisputed leader in all content analytics, however developers will often enjoy the usage of open source tools like Piwik, for those that use Medium there is also built in visitor analytics, that while simple, are useful.

---

## What is lacking in developer content?

I’ve read a lot of developer material, specially in the tutorial space, and I feel a severe lack of TDD oriented content. Most languages and frameworks getting started guides will not use TDD, even if it is a highly regarded practice within the community(such as the Rails Blog Tutorial), in fact the only one I’ve found to have a TDD mentality was Hanami’s guide, many blog posts also do not cover the testing parts and just skip it straight to building stuff, this is fine for simple projects, but if you actually want to learn something new, jump starting TDD could be a huge difference.

---

In a world where new open source tools are constantly being published, this presents a great opportunity for those who can write about it(just look at Yarn!), next time you’re picking up something new, try and tell your experience with it, you won’t just be promoting yourself but helping out the developer community just as much as those who create whatever you’re using.
